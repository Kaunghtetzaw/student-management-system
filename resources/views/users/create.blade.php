@extends('layouts.admin-layout')

@section('content')
    <h3 class="text-center">{{ $form_name }}</h3>
    <br>

    <x-forms::form-tag :attrs="[
        'class' => 'user-create-form',
        'id' => 'user-create-form-id',
        'action' => $store_route,
        'method' => $method,
        'formName' => 'user-create-form',
    ]">
        <x-forms::text-input :attrs="[
            'name' => 'name',
            'id' => 'name',
            'class' => '',
            'value' => isset($user) ? $user->name : old('name'),
            'placeholder' => '',
            'label' => 'Name',
            'required' => 'yes',
        ]" />

        <x-forms::text-input :attrs="[
            'name' => 'email',
            'id' => 'email',
            'class' => '',
            'value' => isset($user) ? $user->email : old('email'),
            'placeholder' => '',
            'label' => 'Email',
            'required' => 'yes',
        ]" />

        <x-forms::select-with-key-value :attrs="[
            'name' => 'role',
            'selected' => isset($user) ? $user->role : old('role'),
            'placeholder' => '',
            'label' => 'Role',
            'required' => 'yes',
            'list' => ['admin' => 'Admin', 'student' => 'Student'],
        ]" />

        <x-forms::password-input :attrs="[
            'name' => 'password',
            'id' => 'password',
            'class' => '',
            'value' => '',
            'placeholder' => '',
            'label' => 'Password',
            'required' => 'yes',
        ]" />

        <x-forms::password-input :attrs="[
            'name' => 'password_confirmation',
            'id' => 'password_confirmation',
            'class' => '',
            'value' => '',
            'placeholder' => '',
            'label' => 'Confirm Password',
            'required' => 'yes',
        ]" />

        <button type="submit" class="btn btn-success">{{ $button }}</button>
    </x-forms::form-tag>
@endsection
