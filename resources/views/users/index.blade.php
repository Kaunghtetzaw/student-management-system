@extends('layouts.admin-layout')

@section('content')
    <h3 class="text-center">Users</h3>
    <br>
    <a href="{{ $create_route }}" class="btn btn-success">New User</a>
    <hr>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Role</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users ?? [] as $key => $user)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                      <span class="badge bg-primary rounded-pill"> {{ $user->role }}</span>
                    </td>
                    <td>
                        <x-forms::form-tag :attrs="[
                            'class' => 'delete-form',
                            'id' => 'delete-form-id',
                            'action' => route($delete_route, $user),
                            'method' => 'post',
                            'formName' => 'delete-form',
                        ]">
                            @method('DELETE')
                            @csrf
                            <a href="{{ route($edit_route, $user) }}" class="btn btn-info">Edit</a>
                            <input class="btn btn-danger" type="submit" value="Delete" />
                        </x-forms::form-tag>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
