@extends('errors::minimal')
@section('title', 'Page Expired')
@section('code', '419')
@section('code-message','Page Expired')
@section('message','The page you’re working is Expired. Please refresh or login again!')

