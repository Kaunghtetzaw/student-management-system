@extends('errors::minimal')
@section('title', 'Server Error')
@section('code', '500')
@section('code-message','Server Error.')
@section('message','There was problems on Server. Please Contact Customer Support! '.env('SUPPORT_PHONE',+959790800373))
