<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// for admin
Route::group(['middleware' => ['auth:web','is_admin']], function () {

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('admin');
    Route::get('/user/index', [App\Http\Controllers\Admin\UserController::class, 'index'])->name('user.index');
    Route::get('/user/create', [App\Http\Controllers\Admin\UserController::class, 'create'])->name('user.create');
    Route::post('/user/store', [App\Http\Controllers\Admin\UserController::class, 'store'])->name('user.store');
    Route::get('/user/edit/{user}', [App\Http\Controllers\Admin\UserController::class, 'edit'])->name('user.edit');
    Route::post('/user/update/{user}', [App\Http\Controllers\Admin\UserController::class, 'update'])->name('user.update');
    Route::delete('/user/delete/{user}', [App\Http\Controllers\Admin\UserController::class, 'delete'])->name('user.delete');

});

// for student
Route::group(['middleware' => ['auth:web','is_student']], function () {

    Route::get('/', function () {
        return view('welcome');
    })->name('student');

});

Auth::routes();




