<?php

namespace App\Repositories;

use App\Interfaces\UserInterface;
use App\Services\UserService;

class UserRepository implements UserInterface
{

    /**
     * @var userService
     */
    private $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * get all user
     * 
     * @return Array
     */
    public function getAllUsers()
    {
        $users = $this->userService->getAllUsers();

        $data = [
            'users' => $users,
            'edit_route' => 'user.edit',
            'delete_route' => 'user.delete',
            'create_route' => route('user.create')
        ];

        return $data;
    }

    /**
     * get create data
     * 
     * @return Array
     */
    public function createData()
    {
        $data = [
            'form_name' => 'User Create',
            'store_route' => route('user.store'),
            'method' => 'POST',
            'button' => 'Create'
        ];

        return $data;
    }

    /**
     * store data
     * 
     * @return Array
     */
    public function store($request)
    {
        $data = $request->all();

        return $this->userService->store($data);
    }

    /**
     * edit data
     * 
     * @return Array
     */
    public function editData($user)
    {
        $data = [
            'user' => $user,
            'form_name' => 'User Edit',
            'store_route' => route('user.update', $user),
            'method' => 'POST',
            'button' => 'Update'
        ];

        return $data;
    }

    /**
     * update data
     * 
     * @return Array
     */
    public function update($request, $user)
    {
        $data = $request->except('password');

        return $this->userService->update($data, $user);
    }


    /**
     * delete data
     * 
     * @return Array
     */
    public function delete($user)
    {
        return $this->userService->delete($user);
    }
}
