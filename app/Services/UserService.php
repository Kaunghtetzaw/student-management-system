<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\DB;

class UserService
{
    /**
     * get all user
     * 
     * @return collection
     */
    public function getAllUsers()
    {
        $users = User::paginate(25);

        return $users;
    }

    /**
     * store user
     */
    public function store($data)
    {
        try {
            DB::beginTransaction();
            User::create($data);
            
            DB::commit();
            return ['status' => 'success', 'message' => 'success'];
        } catch (\Throwable $th) {
            DB::rollback();
            return ['status' => 'error', 'messsage' => 'something went wrong'];
        }
    }

    /**
     * update user
     */
    public function update($data, $user)
    {
        try {
            DB::beginTransaction();
            $user->update($data);
            DB::commit();
            
            return ['status' => 'success', 'message' => 'success'];
        } catch (\Throwable $th) {
            DB::rollback();
            return ['status' => 'error', 'message' => 'something went wrong'];
        }
    }

    /**
     * delete user
     */
    public function delete($user)
    {
        try {
            DB::beginTransaction();
            $user->delete();
            DB::commit();

            return ['status' => 'success', 'message' => 'success'];
        } catch (\Throwable $th) {
            DB::rollback();

            return ['status' => 'error', 'message' => 'something went wrong'];
        }
    }
}