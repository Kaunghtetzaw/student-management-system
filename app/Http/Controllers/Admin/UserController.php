<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\StoreRequest;
use App\Interfaces\UserInterface;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserInterface
     */
    private $userInterface;

    /**
     * @param UserInterface $userInterface
     */
    public function __construct(UserInterface $userInterface)
    {
        $this->userInterface = $userInterface;
    }

    /**
     * user index
     * 
     * @return view
     */
    public function index()
    {
        $data = $this->userInterface->getAllUsers();

        return view('users.index', $data);
    }

    /**
     * user create
     * 
     * @return view
     */
    public function create()
    {
        $data = $this->userInterface->createData();

        return view('users.create', $data);
    }

    /**
     * user store
     * 
     * @return view
     */
    public function store(StoreRequest $request)
    {
        $data = $this->userInterface->store($request);

        return redirect()->route('user.index')->with($data['status'], $data['message']);
    }

    /**
     * user edit
     * 
     * @return view
     */
    public function edit(User $user)
    {
        $data = $this->userInterface->editData($user);

        return view('users.create', $data);
    }

    /**
     * user update
     * 
     * @return view
     */
    public function update(StoreRequest $request, User $user)
    {
        $data = $this->userInterface->update($request, $user);

        return redirect()->route('user.index')->with($data['status'], $data['message']);
    }

    /**
     * user delete
     * 
     * @return view
     */
    public function delete(User $user)
    {
        $data = $this->userInterface->delete($user);

        return redirect()->route('user.index')->with($data['status'], $data['message']);
    }
}
